<?php

namespace Tools\CodeBase;

use PhpParser\Node;
use PhpParser\NodeFinder;

class FunctionToTest {
	private $rootNode;

	public function __construct( $rootNode ) {
		$this->rootNode = $rootNode;
	}

	public function name() {
		return $this->rootNode->name->name;
	}

	public function arguments(): array {
		return [
			[
				'type' => 'Title',
				'optional' => false,
				'name' => 'title',
			],
			[ 'type' => 'Context', 'optional' => true, 'name' => 'context', 'dependencies' => [] ],
		];
	}

	public function result() {
		return [];
	}

	public function context() {
		$res = [
			'arguments' => $this->arguments(),
			'this' => [ 'getPageLanguage', 'getLanguageConverter' ],
			'global' => [ 'MediaWikiServices' => [ 'getLanguageFactory' ] ],
			'conditions' => [ [ 'title', 'getNamespace' ] ],
			'dependencies' => [ [ 'namespace', 'langWithVariants' ] ]
	];

		return $res;
	}

	public function className() {
		return 'ContentHandler';
	}
}

// $pageLang = $this->getPageLanguage( $title, $content );

// if ( $title->getNamespace() !== NS_MEDIAWIKI ) {
// // If the user chooses a variant, the content is actually
// // in a language whose code is the variant code.
// $variant = $this->getLanguageConverter( $pageLang )->getPreferredVariant();
// if ( $pageLang->getCode() !== $variant ) {
// 	$pageLang = MediaWikiServices::getInstance()->getLanguageFactory()
// 		->getLanguage( $variant );
// }
