<?php

namespace Tools;

use Tools\CodeBase\PhpCodeStructure\PhpClass;
use Tools\CodeBase\PhpCodeStructure\PhpMethod;
use Tools\CodeBase\PhpCodeStructure\PhpProperty;
use Tools\CodeBase\PhpCodeStructure\PhpVariable;

class Deserializator {
    public static function arrayToClass( array $class ): PhpClass {
        return PhpClass::constructByArray( $class );
    }

    public static function arrayToMethod( array $method ): PhpMethod {
        return PhpMethod::constructByArray( $method );
    }

    public static function arrayToArgument( array $argument ): PhpVariable {
        return PhpVariable::constructByArray( $argument );
    }

    public static function arrayToProperty( array $property): PhpProperty {
        return PhpProperty::constructByArray( $property );
    }
}