<?php

namespace Tools\CodeBase;

class WithName implements ExtensionsFilter {
	private $name;
	private $parent;

	public function __construct( string $name, ?ExtensionsFilter $parent = null ) {
		$this->parent = $parent ?? new AllExtensions();
		$this->name = $name;
	}

	public function test( Extension $extension ): bool {
		return $this->parent->test( $extension ) && $extension->name() == $this->name;
	}
}
