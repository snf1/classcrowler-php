<?php

namespace Tools\ProjectFilters;

use Tools\CodeBase\Project;

interface ProjectFilter {
	public function test( Project $project ): bool;
}
