<?php

namespace Tools\CodeBase;

class VersionLess implements ExtensionsFilter {
	private $branch;

	private $parent;

	public function __construct( $version, ?ExtensionsFilter $parent = null ) {
		$this->parent = $parent ?? new AllExtensions();
		$this->branch = $branch;
	}

	private function checkVersion( array $versions ) {
		return false;
	}

	public function test( Extension $extension ): bool {
		return $this->parent->test( $extension ) &&
			$this->checkVersion( ( new ExtensionRequires() )->toArray( $extension ) );
	}
}
