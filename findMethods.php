<?php
/**
 * Maintenance script that recursively scans MediaWiki's PHP source tree
 * for methods that use User class and pretty-prints the results.
 *
 * @file
 * @ingroup Maintenance
 * @phan-file-suppress PhanUndeclaredProperty Lots of custom properties
 */

use MongoDB\Client;

require_once __DIR__.'/Maintenance.php';
require_once __DIR__.'/../vendor/autoload.php';

/**
 * A PHPParser node visitor that associates each node with its file name.
 */
class FileNodeVisitor extends PhpParser\NodeVisitorAbstract {
	private $currentFile = null;

	public function enterNode( PhpParser\Node $node ) {
		$retVal = parent::enterNode( $node );
		$node->filename = $this->currentFile;

		return $retVal;
	}

	public function setCurrentFile( $filename ) {
		$this->currentFile = $filename;
	}

	public function getCurrentFile() {
		return $this->currentFile;
	}
}

/**
 * A PHPParser node visitor that finds deprecated functions and methods.
 */
class MethodsInterfaceFinder extends FileNodeVisitor {

	private $currentClass = null;

	private $foundNodes = [];

	public function getFoundNodes() {
		// Sort results by filename, then by name.
		uasort( $this->foundNodes,
			static function ( $a, $b ) {
				return ( $a['filename'].$a['name'] ) <=> ( $b['filename'].$b['name'] );
			} );

		return $this->foundNodes;
	}

	/**
	 * Check whether a function or method includes a call to wfDeprecated(),
	 * indicating that it is a hard-deprecated interface.
	 *
	 * @param PhpParser\Node $node
	 *
	 * @return bool
	 */
	public function isHardDeprecated( PhpParser\Node $node ) {
		if ( !$node->stmts ) {
			return false;
		}
		foreach ( $node->stmts as $stmt ) {
			if ( $stmt instanceof PhpParser\Node\Expr\FuncCall && $stmt->name->toString() === 'user' ) {
				return true;
			}

			return false;
		}
	}


	public function enterNode( PhpParser\Node $node ) {
		$retVal = parent::enterNode( $node );

		if ( $node instanceof PhpParser\Node\Stmt\ClassLike ) {
//			$this->currentClass = $node->name;
			$data = [];
			$data['name'] = $node->name->name;
			if ($node->implements) {
				foreach ( $node->implements as $interface ) {
					$data['implemants'][] = $interface->parts[0];
				}
			} else {
				$data['implemants'] = null;
			}
			$data['extends'] = $node->extends ? $node->extends->parts[0] : null;
			foreach ($node->getProperties() as $index => $property) {
				$data['properties'][$index]['name'] = $property->props[0]->name->name;
				$docComment = $property->getDocComment();
				!$docComment ?: preg_match( '/@var (\w*)/', $docComment->getText(), $matches );
				$type = $property->type ?? $matches ? $matches[1] : null;
				$data['properties'][$index]['type'] = $type;
			}
//			$node->getMethods();
			foreach ( $node->getMethods() as $sequentialNumber => $method ) {
				$data['methods'][$sequentialNumber]['name'] = $method->name->name;
				$data['methods'][$sequentialNumber]['returnType'] = $method->returnType;

				foreach ( $method->getParams() as $paramNumber => $param ) {
					$data['methods'][$sequentialNumber]['arguments'][$paramNumber]['name'] = $param->var->name ?? null;
					if ( $param->type && $param->type->parts ) {
						$data['methods'][$sequentialNumber]['arguments'][$paramNumber]['type'] = null;
						foreach ( $param->type->parts as $index => $partFQN ) {
							$data['methods'][$sequentialNumber]['arguments'][$paramNumber]['type'] .= $index == 0 ? $partFQN : '\\' . $partFQN;
						}
					} else {
						$data['methods'][$sequentialNumber]['arguments'][$paramNumber]['type'] = null;
					}
				}
			}

			$connection = new MongoDB\Client(
				'mongodb+srv://admin:wmf-admin@wmf-cluster.tjsne.mongodb.net/classTreeDB?retryWrites=true&w=majority');
			$collection = $connection->selectCollection('classTreeDB', 'functions');
//			$collection->drop();

			$insertOneResult = $collection->insertOne($data);

		}

//		if ( $node instanceof PhpParser\Node\FunctionLike ) {
//			$docComment = $node->getDocComment();
//			$params = $node->getParams(); // return if no params
//			$user = null;
//			if ( !empty( $params ) ) {
//				foreach ( $params as $param ) {
//					if ( $user ) {
//						continue;
//					}
//					if ( $param->type instanceof PhpParser\Node\Name ) {
//						$user = $param->type->parts[0] == 'User';
//					}
//				}
//			}
//			if ( !$docComment && !$user ) {
//				return;
//			}
//			if ($docComment && !preg_match( '/@param User .*/', $docComment->getText(), $matches ) && !$user ) {
//				return;
//			}
//
//			if ( $node instanceof PhpParser\Node\Stmt\ClassMethod ) {
//				$name = $this->currentClass.'::'.$node->name;
//			} elseif ( $node instanceof \PhpParser\Node\Expr\Closure ) {
//				$name = $this->currentClass->name;
//			} else {
//				$name = $node->name;
//			}
//
//			$this->foundNodes[] = [
//				'filename' => $node->filename,
//				'line' => $node->getLine(),
//				'name' => $name,
//				'fixable' => $this->isHardDeprecated( $node ),
//			];
//		}

		return $retVal;
	}
}

/**
 * Maintenance task that recursively scans MediaWiki PHP files for deprecated
 * functions and interfaces and produces a report.
 */
class FindMethods extends Maintenance {

	public function __construct() {
		parent::__construct();
		$this->addDescription( 'Find methods with class arguments' );
	}

	/**
	 * @return SplFileInfo[]
	 */
	public function getFiles() {
		global $IP;

		$files = new RecursiveDirectoryIterator( $IP.'/includes' );
		$files = new RecursiveIteratorIterator( $files );
		$files = new RegexIterator(
			$files,
			'/\.php$/'
		);

		return iterator_to_array(
			$files,
			false
		);
	}

	public function execute() {
		global $IP;

		$files = $this->getFiles();
		$chunkSize = ceil( count( $files ) / 72 );

		$parser = ( new PhpParser\ParserFactory )->create( PhpParser\ParserFactory::PREFER_PHP7 );
		$traverser = new PhpParser\NodeTraverser;
		$finder = new MethodsInterfaceFinder;
		$traverser->addVisitor( $finder );

		$fileCount = count( $files );

		for ( $i = 0; $i < $fileCount; $i++ ) {
			$file = $files[$i];
			$code = file_get_contents( $file );

			if ( strpos(
					$code,
					'@param User'
				) === -1 ) {
				continue;
			}

			$finder->setCurrentFile(
				substr(
					$file->getPathname(),
					strlen( $IP ) + 1
				)
			);
			$nodes = $parser->parse( $code );
			$traverser->traverse( $nodes );

			if ( $i % $chunkSize === 0 ) {
				$percentDone = 100 * $i / $fileCount;
				fprintf(
					STDERR,
					"\r[%-72s] %d%%",
					str_repeat(
						'#',
						$i / $chunkSize
					),
					$percentDone
				);
			}
		}

		fprintf(
			STDERR,
			"\r[%'#-72s] 100%%\n",
			''
		);

		// Colorize output if STDOUT is an interactive terminal.
		if ( posix_isatty( STDOUT ) ) {
			$versionFmt = "\n* Deprecated since \033[37;1m%s\033[0m:\n";
			$entryFmt = "  %s \033[33;1m%s\033[0m (%s:%d)\n";
		} else {
			$versionFmt = "\n* Deprecated since %s:\n";
			$entryFmt = "  %s %s (%s:%d)\n";
		}

//		$connection = new MongoDB\Client(
//			'mongodb+srv://admin:wmf-admin@wmf-cluster.tjsne.mongodb.net/classTreeDB?retryWrites=true&w=majority'
//		);
//		$collection = $connection->selectCollection(
//			'classTreeDB',
//			'functions'
//		);
//		$collection->drop();
//		$collection->findOne();
		//$db->insertOne();

//		foreach ( $finder->getFoundNodes() as $node ) {
//			$insertOneResult = $collection->insertOne( [
//				'class_name' => $node['name'],
//				'file_name' => $node['filename'],
//				'line_number' => $node['line'],
//			] );
//			$data = $collection->findOne( [ '_id' => $insertOneResult->getInsertedId() ] );
//			printf(
//				$entryFmt,
//				$node['fixable'] ? '+' : '-',
//				$data['class_name'],
//				$data['file_name'],
//				$data['line_number']
//			);
//			printf(
//				$entryFmt,
//				$node['fixable'] ? '+' : '-',
//				$node['name'],
//				$node['filename'],
//				$node['line']
//			);
//		}
//		printf( "\nlegend:\n -: not fixable\n +: fixable\n" );
	}
}

$maintClass = FindMethods::class;
require_once RUN_MAINTENANCE_IF_MAIN;
