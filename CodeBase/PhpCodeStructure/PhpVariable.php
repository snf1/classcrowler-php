<?php

namespace Tools\CodeBase\PhpCodeStructure;

use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Comment\Doc;
use PhpParser\NameContext;
use PhpParser\Node\NullableType;

class PhpVariable {

    /** @var string|null */
    private $name;
    /** @var array */
    private $types;
    
    /** @var Param|null */
    private $node;
    
    /** @var Doc|null */
    private $doc;

    /** @var NameContext|null */
    private $nameContext;

    function __construct() { }

    public static function constructByNode( Param $node, ?Doc $doc, NameContext $nameContext ): PhpVariable {
        $phpVariable = new self();
        $phpVariable->byNode( $node, $doc, $nameContext );
        return $phpVariable;
    }

    public static function constructByArray( $argument ): PhpVariable {
        $phpVariable = new self();
        $phpVariable->byArray( $argument );
        return $phpVariable;
    }

    protected function byNode( Param $node, ?Doc $doc, NameContext $nameContext ) {
        $this->node = $node;
        $this->doc = $doc;
        $this->nameContext = $nameContext;
    }

    protected function byArray( $argument ) {
        $this->name = $argument[ "name" ];
        $this->types = $argument[ "types" ];
    }
    
    function name(): string {
        return $this->name ?? $this->node->var->name;
    }
    /**
     * @return string[]
     */
    function types(): array {
        if (isset($this->types)) {
            return $this->types;
        }
        if (isset($this->node->type)) {
            $type = $this->node->type;
            if ($type instanceof NullableType) {
                $type = $type->type;
            }
            return [$type->__toString()];
        }
        if (isset($this->doc)) {
            $docText = $this->doc->getText();
            $rx = '/@param (.*) \$'. $this->name() . '/';
            if( preg_match( $rx, $docText, $matches )) {
                $types = explode('|', $matches[1]);
                $res = [];
                foreach($types as $type) {
                    $res[] = $this->nameContext->getResolvedClassName(new Name($type))->__toString();
                }
                return $res;
            }
        }
      
        return [];
    }
}