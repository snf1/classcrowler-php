<?php

namespace Tools\CodeBase\PhpCodeStructure;

use PhpParser\Comment\Doc;
use PhpParser\NameContext;
use PhpParser\Node\Name;
use PhpParser\Node\NullableType;
use PhpParser\Node\Stmt\Property;

class PhpProperty {
    /** @var string|null */ 
    private $fullName;

    /** @var PhpClass|null */ 
    private $class;

    /** @var string|null */
    protected $name;

    /** @var array */
    protected $types;

    /** @var Property|null */
    protected $node;

    /** @var Doc|null */
    protected $doc;

    /** @var NameContext|null */
    protected $nameContext;
    /**
     * @var string
     */
    protected $fqnClassName;

    function __construct() {
//        parent::__construct();
    }

    public static function constructByNode( Property $node, ?Doc $doc, NameContext $nameContext, string $fqnClassName): PhpProperty {
        $phpProperty = new self();
        $phpProperty->byNode( $node, $doc, $nameContext, $fqnClassName );
        return $phpProperty;
    }

    public static function constructByArray( $property ): PhpProperty {
        $phpProperty = new self();
        $phpProperty->byArray( $property );
        return $phpProperty;
    }

    protected function byNode( Property $node, ?Doc $doc, NameContext $nameContext, string $fqnClassName ) {
//        parent::byNode( $node, $doc, $nameContext );
        $this->node = $node;
        $this->doc = $doc;
        $this->nameContext = $nameContext;
        $this->fqnClassName = $fqnClassName;
        $this->fullName = $this->fullName();
    }

    protected function byArray( $property ) {
//        parent::byArray( $property );
        $this->name = $property[ "name" ];
        $this->types = $property[ "types" ];
        $this->fullName = $property[ "fullName" ];
    }

    function class(): ?PhpClass {
        return $this->class;
    }

    function name(): string {
        return $this->name ?? $this->node->props[0]->name->name;
    }

    /**
     * @return string[]
     */
    function types(): array {
        if (isset($this->types)) {
            return $this->types;
        }
        if (isset($this->node->type)) {
            $type = $this->node->type;
            if ($type instanceof NullableType) {
                $type = $type->type;
            }
            return [$type->__toString()];
        }
        if (isset($this->doc)) {
            $docText = $this->doc->getText();
            $rx = '/@var (.*?)[ |\n]/';
            if( preg_match( $rx, $docText, $matches )) {
                $types = explode('|', $matches[1]);
                $res = [];
                foreach($types as $type) {
                    $res[] = $this->nameContext->getResolvedClassName(new Name($type))->__toString();
                }
                return $res;
            }
        }

        return [];
    }

    function fullName(): string  {
        if(isset($fullName)) {
            return $this->fullName;
        }

        return $this->fqnClassName . '::' . $this->name();
    }
}