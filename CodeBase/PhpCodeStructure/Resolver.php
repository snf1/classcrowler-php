<?php

namespace Tools\CodeBase\PhpCodeStructure;

use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\StaticCall;

class Resolver
{
    public array $processedClasses;

    public function __construct($processedClasses)
    {
        $this->processedClasses = $processedClasses;
    }

    /**
     * @param string $haystack Possible class where we will search param or method
     * @param string $needle Wat to search in haystack
     *
     * @return mixed
     */
    public function getReturnType(string $haystack, string $needle, $expr)
    {
        if (isset($this->processedClasses)) {
            if ($expr instanceof PropertyFetch) {
                return $this->processedClasses[$haystack]->properties()[$needle]->types()[0];
            } elseif ($expr instanceof MethodCall or $expr instanceof StaticCall) {
                if (isset($this->processedClasses[$haystack])) {
                    try {
                        return $this->processedClasses[$haystack]->methods()[$needle]->returnTypes()[0];
                    } catch (\Exception $e) {
                        print $e->getMessage();

                        return null;
                    }
                } else {
                    return null;
                }
            }
        }

        return 'You not suppose to be here';
    }
}