<?php
namespace Tools\CodeBase\PhpCodeStructure;

use Exception;
use PhpParser\Node\Name;
use PhpParser\NameContext;
use PhpParser\Node\Stmt\ClassLike;
use Tools\CodeBase\Files\GenericFile;

class PhpClass {
    /** @var string|null $name */
    private $name;
    /** @var string|null $fullName */
    private $fullName;
    /** @var string[] $implements */
    private $implements;
    /** @var string[] $extends */
    private $extends;
    /** @var array $inherits */
    private $inherits;
    /** @var array $derived */
    private $derived;
    /** @var PhpMethod[] $methods */
    private $methods;
    /** @var PhpProperty[] $methods */
    private $properties;
    /** @var string $extensionName */
    private $extensionName;

    /** @var ClassLike|null $node */
    private $node;

    /** @var  NameContext|null $nameContext */
    private $nameContext;

    /** @var  GenericFile */
    private $file;

    function __construct() { }

    public static function constructByNode( ClassLike $node, GenericFile $file, NameContext $nameContext ): PhpClass {
        $phpClass = new self();
        $phpClass->byNode( $node,  $file, $nameContext );
        return $phpClass;
    }

    /**
     * @return PhpClass
     */
    public static function constructByArray(  $class ): PhpClass {
        $phpClass = new self();
        $phpClass->byArray( $class );
        return $phpClass;
    }

    protected function byNode( ClassLike $node, GenericFile $file, NameContext $nameContext ) {
        $this->node = $node;
        $this->file = $file;
        $this->nameContext = $nameContext;
        $this->file = $file;
    }

    protected function byArray(  $class ) {
        $this->name = $class[ "name" ];
        $this->fullName = $class[ "fullName" ];
        $this->implements = $class[ "implements" ];
        $this->extends = $class[ "extends" ];
        $this->inherits = $class[ "inherits" ];
        $this->derived = $class[ "derived" ];
        $this->extensionName = $class[ "extension" ];

        $this->methods = [];
        foreach( $class[ "methods" ] as $methodName => $method ) {
            $this->methods[$methodName] = PhpMethod::constructByArray($method);
        }

        foreach( $class[ "properties" ] as $methodName => $method ) {
            $this->methods[$methodName] = PhpProperty::constructByArray($method);
        }
    }

    /**
     * @return PhpMethod[]
     */
    function methods(): array {
        if(isset($this->methods)) {
            return $this->methods;
        }
        $phpMethods = [];
        foreach ( $this->node->getMethods() as $method ) {
            $newMethod = PhpMethod::constructByNode( $method, $this, $this->file, $this->nameContext );
            $phpMethods[$newMethod->name()] = $newMethod;
        }

        return $phpMethods;
    }

    /**
     * @return string[]
     */
    function implements(): array {
        if(isset($this->implements)) {
            return $this->implements;
        }
        if ( !isset($this->node->implements) ) {
            return [];
        }

        $implements = [];
        foreach ( $this->node->implements as $interface ) {
            $implements[] = $interface->__toString();
        }
        return $implements;
    }

    /**
     * @return string[]
     */
    function extends(): array {
        if(isset($this->extends)) {
            return $this->extends;
        }
        if (!isset($this->node->extends)) {
            return [];
        }
        if ($this->node->extends instanceof Name) {
            $res = $this->node->extends->__toString();
            return [ $res ];
        }
        $extentions = [];
        foreach ( $this->node->extends as $extends ) {
            $extentions[] = $extends->__toString();
        }
        return $extentions;
    }

    /**
     * @return string
     */
    function name(): string {
        if(isset($this->name)) {
            return $this->name;
        }
        return $this->node->name->name;
    }

    /**
     * @return string
     */
    function fullName(): string {
        if(isset($this->fullName)) {
            return $this->fullName;
        }
        return $this->node->namespacedName;
    }

    /**
     * @return array|PhpProperty[]|null
     */
    function properties(): ?array {
        if(isset($this->properties)) {
            return $this->properties;
        }

        $phpProperties = [];
        foreach ($this->node->getProperties() as $property) {
            $newProperty = PhpProperty::constructByNode($property, $property->getDocComment(), $this->nameContext, $this->fullName());
            $phpProperties[$newProperty->name()] = $newProperty;
        }

        return $phpProperties;
    }
    function fileName(): string {
       return $this->file->path();
    }

    function extensionName(): string {
        if(isset($this->extensionName)) {
            return $this->extensionName;
        }

        return $this->file->project()->name();
    }

    function inherits(): array {
        return $this->inherits;
    }

    function derived(): array {
        return $this->derived;
    }
}
