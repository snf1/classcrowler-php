<?php

namespace Tools\CodeBase\Projects;

use Exception;
use FilterIterator;
use Tools\CodeBase\Project;
use Tools\ProjectFilters\AllProjectFilter;
use Tools\ProjectFilters\CombineProjectFilter;
use Tools\ProjectFilters\ProjectFilter;
use Traversable;


class ProjectFilterIterator extends FilterIterator {

	private $projectFilter = null;

	public function __construct( Traversable $projects, $projectFilter = null ) {
		parent::__construct( $projects );
		$this->projectFilter = $projectFilter ?? new AllProjectFilter();
	}

	public function filter( object $filter ) {
		if ( $filter instanceof ProjectFilter ) {
			return new ProjectFilterIterator(
				$this->getInnerIterator(),
				new CombineProjectFilter( $filter, $this->projectFilter )
			);
		}
		throw new Exception( 'Invalid filter' );
	}

	public function accept(): bool {
		$current = parent::current();
		return ( $current instanceof Project &&
			$this->projectFilter->test( $current ) );
	}
}
