<?php

namespace Tools\CodeBase\Projects;

use FilterIterator;
use Tools\CodeBase\Extension;

class ExtensionIterator extends FilterIterator {

	public function accept(): bool {
		$current = parent::current();
		return !parent::current()->isDot() && parent::current()->isDir() && parent::current()->getFilename() != '.git';
	}

	public function current() {
		return new Extension( parent::current()->getPathname() );
	}
}
