<?php

namespace Tools\CodeBase;

use AppendIterator;
use Iterator;
use RecursiveIteratorIterator;
use RegexIterator;
use RecursiveDirectoryIterator;
use Tools\CodeBase\Files\GenericFileIterator;
use Traversable;

class Extension implements Project {
	private $basePath;

	public function __construct( string $basePath ) {
		$this->basePath = $basePath;
	}

	public function name(): string {
		$dirParts = explode( '/', 	$this->basePath );
		$pos = array_search( 'extensions', $dirParts );
		if ( $pos === false ) {
			return "Unknown";
		}
		if ( $pos >= count( $dirParts ) - 1 ) {
			return "Unknown";
		}
		return $dirParts[ $pos + 1 ];
	}

	public function path(): string {
		return $this->basePath;
	}

	private function extensionFilepath(): string {
		$includes = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator( $this->basePath, RecursiveDirectoryIterator::SKIP_DOTS )
		);
		$includes = new RegexIterator( $includes, '/extension\.json$/' );
		$extensionFiles = iterator_to_array( $includes, false );
		if ( !empty( $extensionFiles ) ) {
			return $extensionFiles[0]->getPathname();
		}
		return $this->path() . '/extension.json';
	}

	public function hasExtensionFile(): bool {
		return file_exists( $this->extensionFilepath() );
	}

	public function extensionJson(): array {
		$extensionJsonPath = $this->extensionFile();
		if ( !file_exists( $extensionJsonPath ) ) {
			return [];
		}
		$json = file_get_contents( $extensionJsonPath );
		return json_decode( $json, true );
	}

	public function version(): string {
		return $this->extensionJson()['version'] ?? 'Unknown';
	}

	public function files(): Iterator {
        //previous solution, will review later
//		$files = new RecursiveDirectoryIterator( $this->basePath );
//		return new GenericFileIterator( $this,  $files );

        $files = new AppendIterator();

        if (file_exists($this->basePath . '/includes')) {
            $extension = new RecursiveDirectoryIterator( $this->basePath . '/includes' );
            $files->append( new RecursiveIteratorIterator( $extension ) );

            return new GenericFileIterator( $this, $files );
        }

        return $files;
    }
}
