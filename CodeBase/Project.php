<?php

namespace Tools\CodeBase;

use Traversable;

interface Project {
	public function files(): Traversable;

	public function version(): string;

	public function name(): string;

	public function path(): string;

	public function extensionJson(): array;
}
