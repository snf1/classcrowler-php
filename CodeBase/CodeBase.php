<?php

namespace Tools\CodeBase;

// use Exception;
// use Traversable;
use AppendIterator;
use RecursiveArrayIterator;
use RecursiveFilterIterator;
// use FilterIterator;
// use RecursiveArrayIterator;
// use RecursiveFilterIterator;
// use RecursiveIteratorIterator;
// use Tools\FileFilters\FileFilter;
// use Tools\FileFilters\AnyFileFilter;
// use Tools\CodeBase\Files\FileFilterIterator;
use Tools\CodeBase\Files\FilesFilterIterator;
use Tools\CodeBase\Projects\ExtensionIterator;
use Tools\CodeBase\Projects\ProjectFilterIterator;
use Traversable;

// use Tools\ProjectFilters\CombineProjectFilter;

// class PhpCodeInfo extends RecursiveFilterIterator {

// 	private $projectFilter = null;
// 	private $fileFilter = null;

// 	private $projects;



// 	public function filter($flt): PhpCode {
// 		if ($flt instanceof ProjectFilter) {

// 		} elseif ($flt instanceof FilesFilter) {

// 		}
// 	}
// 	public function accept(){
// 		return true;
// 	}
// }

class CodeBase {
	public static function files( array $projects ): FilesFilterIterator {
		$all = new AppendIterator();
		foreach ( $projects as $project ) {
            if($project instanceof ExtensionIterator) {
                foreach ($project as $extension) {
                    $all->append($extension->files());
                }
            }else {
			    $all->append( $project->files() );
            }
		}
		return new FilesFilterIterator( $all );
	}

	public static function projects( array $projects ): ProjectFilterIterator {
		$all = new AppendIterator();
		foreach ( $projects as $project ) {
			$all->append( $project );
		}

		return new ProjectFilterIterator( $all );
	}
}
