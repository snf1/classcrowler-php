<?php

namespace Tools\CodeBase;

use DirectoryIterator;
use Tools\CodeBase\Projects\ExtensionIterator;

class Projects {
	public static function core( $basePath ): Core {
		return  new Core( $basePath ) ;
	}

	public static function extensions( $basePath ): ExtensionIterator {
		return new ExtensionIterator( new DirectoryIterator( $basePath, ) );
	}

	public static function extension( $basePath ): Extension {
			return  new Extension( $basePath ) ;
	}
}
