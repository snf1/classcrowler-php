<?php

namespace Tools\CodeBase\Files;

use Tools\CodeBase\File;
use Tools\CodeBase\Project;

class GenericFile implements File {

	private $basePath;
	private $project;

	public function __construct( string $basePath, Project $project ) {
		$this->basePath = $basePath;
		$this->project = $project;
	}

	public function content(): string {
		return file_get_contents( $this->basePath );
	}
	public function lines(int $start, int $end) {
		$lines = explode("\n", $this->content());
		$res = "";
		for($line = $start-1; $line < $end; $line++) {
			if ($line>= count($lines)) {
				break;
			}
			$res .= $lines[$line] . "\n";
		}
		return $res;
	}

	public function path(): string {
		return $this->basePath;
	}

	public function project(): Project {
		return $this->project;
	}
	
	public function name(): string {
		return "";
	}
}
