<?php

namespace Tools\CodeBase\Files;

use RecursiveIterator;
use Tools\CodeBase\Project;

class PhpFileIterator extends GenericFileIterator {

	public function __construct( Project $project, RecursiveIterator $iterator) {
		parent::__construct( $project, $iterator );
	}

	public function accept() {
        $current = $this->current();
		return in_array(
            $current->getFilename(),
            array( $current->type() ),
            true
        );
    
	}

	public function current() {
		return new PhpFile( parent::current()->getPathname(), $this->project );
	}
}