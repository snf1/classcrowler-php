<?php

namespace Tools;

use Tools\CodeBase\PhpCodeStructure\PhpClass;
use Tools\CodeBase\PhpCodeStructure\PhpMethod;
use Tools\CodeBase\PhpCodeStructure\PhpProperty;
use Tools\CodeBase\PhpCodeStructure\PhpVariable;

class Serializator {
    public static function classToArray( PhpClass $phpClass, array $inherits, array $derived, array $overrides, array $overridden ): array {
        $data = [];
        $data[ "name" ] = $phpClass->name();
        $data[ "fullName" ] = $phpClass->fullName();
        $data[ "implements" ] = $phpClass->implements();
        $data[ "extends" ] = $phpClass->extends();
        $data[ "inherits" ] = $inherits[ $phpClass->fullName() ];
        $data[ "derived" ] = $derived[ $phpClass->fullName() ];
        $data[ "methods" ] = array_map( function($method) use ($overrides, $overridden) {
            return self::methodToArray($method, $overrides, $overridden);
        }, $phpClass->methods() );
        $data[ "properties" ] = array_map( 'Tools\\Serializator::propertyToArray', $phpClass->properties() );
        $data[ "extension" ] = $phpClass->extensionName();
        return $data;
    }

    public static function methodToArray( PhpMethod $phpMethod, array $overrides, array $overridden ): array {
        $data = [];
        $data[ "name" ] = $phpMethod->name();
        $data[ "code" ] = $phpMethod->code();
        $data[ "fullName" ] = $phpMethod->fullName();
        $data[ "overrides" ] = $overrides[$phpMethod->fullName()];
        $data[ "overridden" ] = $overridden[$phpMethod->fullName()];
        $data[ "returnTypes" ] = $phpMethod->returnTypes();
		$data = array_merge($data, $phpMethod->tags());
		$data[ "hardDeprecated" ] = $phpMethod->hardDeprecated();
        $data[ "arguments" ] = array_map( 'Tools\\Serializator::argumentToArray', $phpMethod->arguments() );
        $data[ "extension" ] = $phpMethod->extensionName();
        $data[ "callees" ] = $phpMethod->getCallees();
        $data[ "callers" ] = $phpMethod->getCallers();
        return $data;
    }

    public static function argumentToArray( PhpVariable $variable ): array {
        $data = [];
        $data[ "name" ] = $variable->name();
        $data[ "types" ] = $variable->types();
        return $data;
    }

    public static function propertyToArray( PhpProperty $property ): array {
        $data = [];
        $data[ "name" ] = $property->name();
        $data[ "types" ] = $property->types();
        $data[ "fullName" ] = $property->fullName();
        return $data;
    }
}
