<?php
declare(strict_types=1);
use Tools\CodeBase\Project;
use PHPUnit\Framework\TestCase;
use Tools\CodeBase\Files\PhpFile;

final class PhpClassTest extends TestCase {
    
    public function testPhpClassHandlesClass(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestClass.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];

        $this->assertEquals(3, count($phpClass->methods()));
        $this->assertEquals([], $phpClass->implements());
        $this->assertEquals([], $phpClass->extends());
    }

    public function testPhpClassHandlesClassWithExtends(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestClassWithExtends.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];

        $this->assertEquals(1, count($phpClass->methods()));
        $this->assertEquals([], $phpClass->implements());
        $this->assertEquals(["MessageLocalizer"], $phpClass->extends());
    }

    public function testPhpClassHandlesInterface(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestInterface.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];

        $this->assertEquals(1, count($phpClass->methods()));
        $this->assertEquals([], $phpClass->implements());
        $this->assertEquals([], $phpClass->extends());
    }
    public function testPhpClassHandlesInterfaceWithExtends(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestInterfaceWithExtends.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];

        $this->assertEquals(1, count($phpClass->methods()));
        $this->assertEquals([], $phpClass->implements());
        $this->assertEquals([ 'MessageLocalizer', 'CsrfTokenSetProvider' ], $phpClass->extends());
    }

    public function testPhpClassHandlesClassInNamespace(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestClassInNamespace.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];


        $this->assertEquals("TestNamespace\ContextSource", $phpClass->fullName());

        $this->assertEquals(1, count($phpClass->methods()));
        $this->assertEquals([], $phpClass->implements());
        $this->assertEquals([ 'TestNamespace2\MessageLocalizer' ], $phpClass->extends());
    }

}