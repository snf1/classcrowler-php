<?php
declare(strict_types=1);
use Tools\CodeBase\Project;
use PHPUnit\Framework\TestCase;
use Tools\CodeBase\Files\PhpFile;

final class PhpMethodTest extends TestCase {
    
    public function testPhpMethodHandleTypeHints(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestMethod.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];
        $phpMethod = $phpClass->methods()['publicMethodWithHints'];
        $this->assertNotNull($phpMethod);
        $arguments = $phpMethod->arguments();
        $this->assertEquals(2, count($arguments));
        $param1 = $arguments[0];
        $this->assertSame('param1', $param1->name());
        $this->assertSame(['string'], $param1->types());

        $param2 = $arguments[1];
        $this->assertSame('param2', $param2->name());
        $this->assertSame(['TestNamespace\SimpleClass'], $param2->types());
    }

    public function testPhpMethodHandleDoctypes(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestMethod.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];
        $phpMethod = $phpClass->methods()['publicMethodWithDocTypes'];
        $this->assertNotNull($phpMethod);
        $arguments = $phpMethod->arguments();
        $this->assertEquals(2, count($arguments));

        $param1 = $arguments[0];
        $this->assertSame('param1', $param1->name());
        $this->assertSame(['string'], $param1->types());

        $param2 = $arguments[1];
        $this->assertSame('param2', $param2->name());
        $this->assertSame(['TestNamespace\SimpleClass'], $param2->types());
    }

    public function testPhpMethodHandleWithNullable(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestMethod.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];
        $phpMethod = $phpClass->methods()['publicMethodWithNullable'];
        $this->assertNotNull($phpMethod);
        $arguments = $phpMethod->arguments();
        $this->assertEquals(1, count($arguments));

        $param1 = $arguments[0];
        $this->assertSame('param1', $param1->name());
        $this->assertSame(['TestNamespace\SimpleClass'], $param1->types());
        //$code = $phpMethod->code();
    }
}
