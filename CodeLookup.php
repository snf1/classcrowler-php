<?php

require_once __DIR__ . '/vendor/autoload.php';

use MongoDB\Client;
use Tools\CodeBase\PhpCodeStructure\PhpMethod;
use Tools\Serializator;
use Tools\TestSettings;
use Tools\CodeBase\Core;
use Tools\ExceptionHandler;
use Tools\CodeBase\CodeBase;
use Tools\CodeBase\Projects;
use Tools\CodeBase\Files\PhpFile;
use Tools\FileFilters\PhpFileFilter;
use Tools\CodeBase\PhpCodeStructure\PhpClass;

/**
 * Maintenance script that recursively scans MediaWiki's PHP source tree
 * for deprecated functions and methods and pretty-prints the results.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Maintenance
 * @phan-file-suppress PhanUndeclaredProperty Lots of custom properties
 */

// namespace Tools\CodeBase;

// require_once __DIR__ . '/../Maintenance.php';
//

// use Maintenance;


/**
 * A PHPParser node visitor that associates each node with its file name.
 */
// class FileAwareNodeVisitor extends NodeVisitorAbstract {
// 	private $currentFile = null;

// 	public function enterNode( PhpParser\Node $node ) {
// 		$retVal = parent::enterNode( $node );
// 		$node->filename = $this->currentFile;
// 		return $retVal;
// 	}

// 	public function setCurrentFile( $filename ) {
// 		$this->currentFile = $filename;
// 	}

// 	public function getCurrentFile() {
// 		return $this->currentFile;
// 	}
// }

/**
 * A PHPParser node visitor that finds deprecated functions and methods.
 */
// class DeprecatedInterfaceFinder extends FileAwareNodeVisitor {

// 	private $currentClass = null;

// 	private $foundNodes = [];
// 	private $usages = [];

// 	public function getFoundNodes() {
// 		return $this->foundNodes;
// 	}

// 	/**
// 	 * Check whether a function or method includes a call to wfDeprecated(),
// 	 * indicating that it is a hard-deprecated interface.
// 	 * @param PhpParser\Node $node
// 	 * @return bool
// 	 */
// 	public function isUsed( PhpParser\Node $node ) {
// 		if ( !$node->stmts ) {
// 			return false;
// 		}
// 		foreach ( $node->stmts as $stmt ) {
// 			if (
// 					$stmt instanceof PhpParser\Node\Stmt\Expression &&
// 					$stmt->expr instanceof PhpParser\Node\Expr\FuncCall &&
// 					$stmt->expr->name instanceof PhpParser\Node\Name

// 			) {
// 				if ( $stmt->expr->name->toString() === 'wfDeprecated' ) {
// 					if ( count( $stmt->expr->args ) < 2 ) {
// 						return 'none';
// 					}
// 					if ( $stmt->expr->args[1]->value instanceof PhpParser\Node\Scalar\String_ ) {
// 						return $stmt->expr->args[1]->value->value;
// 					}
// 				}
// 			}
// 			return false;
// 		}
// 	}

// 	public function setCurrentFile( $filename ) {
// 		parent::setCurrentFile( $filename );
// 		$this->usages = [];
// 	}

// 	private function isWikiNamespace( $namespace ) {
// 		return count( $namespace ) === 1 || $namespace[0] === 'MediaWiki';
// 	}

// 	private function isFromWiki( $classParts ) {
// 		return $this->usages[ end( $classParts ) ] ?? true;
// 	}

// 	private function extensionName( $filepath ) {
// 		$pathParts = pathinfo( $filepath );
// 		$dirParts = explode( '/', $pathParts['dirname'] );
// 		$pos = array_search( 'extensions', $dirParts );
// 		if ( $pos === false ) {
// 			return "Core";
// 		}
// 		if ( $pos >= count( $dirParts ) - 1 ) {
// 			return "Core";
// 		}
// 		return $dirParts[$pos + 1];
// 	}

// 	public function enterNode( PhpParser\Node $node ) {
// 		$retVal = parent::enterNode( $node );

// 		if ( $node instanceof PhpParser\Node\Stmt\UseUse ) {
// 			$this->usages [ end( $node->name->parts ) ] =
// 				$this->isWikiNamespace( $node->name->parts );
// 		}

// 		if ( $node instanceof PhpParser\Node\Expr\New_ ) {
// 			if ( $node->class instanceof PhpParser\Node\Name ) {
// 				if ( $node->class->toString() === 'Parser' ) {
// 					if ( $this->isFromWiki( $node->class->parts ) ) {
// 						$extensionName = $this->extensionName( $node->filename );
// 						if ( !isset( $this->foundNodes[ $extensionName ] ) ) {
// 							$this->foundNodes[$this->extensionName( $node->filename ) ] = [];
// 						}

// 						if ( !isset( $this->foundNodes[ $extensionName ][$node->filename] ) ) {
// 							$this->foundNodes[ $extensionName ][$node->filename] = [];
// 						}
// 						$this->foundNodes[ $extensionName ][$node->filename] [] = [
// 							'line'     => $node->getLine(),
// 							'startFilePos'     => $node->getStartFilePos(),
// 							'endFilePos'     => $node->getEndFilePos(),
// 							'name'     => $node->class->toString(),
// 						];
// 					}
// 				}
// 			}
// 		}
// 		return $retVal;
// 	}
// }

// /**
//  * Maintenance task that recursively scans MediaWiki PHP files for deprecated
//  * functions and interfaces and produces a report.
//  */

// class ExceptRegexIterator extends \RegexIterator {
// 	public function accept() {
// 		return !parent::accept();
// 	}
// }

// class FileInfo {

// }
// class GitBlock {
// 	const MERGED = 1;
// 	const UNMERGED = 2;
// 	const ALL = 0;

// 	private $basePath;
// 	public function __construct( string $gitPath ) {
// 		$this->basePath = getcwd();
// 		if ( !chdir( $gitPath ) ) {
// 			print( $gitPath );
// 		}
// 	}

// 	private function branches( $merged ): array {
// 		$command = 'git branch';
// 		switch ( $merged ) {
// 			case self::MERGED: $command .= ' --merged master';
// 			case self::UNMERGED: $command .= ' --no-merged master';
// 			default: break;
// 		}

// 		if ( ( $res = shell_exec( $command ) ) == null ) {
// 			return [];
// 		}
// 		$branches = explode( "\n", trim( $res ) );
// 		$result = [];
// 		$current = [];
// 		foreach ( $branches as $branch ) {
// 			if ( substr( $branch, 0, 2 ) == '* ' ) {
// 				$current [] = substr( $branch, 2 );
// 			} else {
// 				$result [] = $branch;
// 			}
// 		}
// 		return array_merge( $result, $current );
// 	}

// 	public function checkout( $branch ): GitBlock {
// 		if (
// 			$branch == 'master'
// 			|| array_search( $branch, $this->branches( self::ALL ) ) !== false
// 		) {
// 			$command = 'git checkout ' . $branch;
// 		} else {
// 			$command = 'git checkout -b ' . $branch;
// 		}
// 		$res = shell_exec( $command );
// 		return $this;
// 	}

// 	public function pull() {
// 		$res = shell_exec( 'git pull' );
// 		return $this;
// 	}

// 	public function isUnderReview( $branch ): bool {
// 		$branches = $this->branches( self::UNMERGED );
// 		return array_search( $branch, $branches ) !== false;
// 	}

// 	public function isMerged( $branch ): bool {
// 		$branches = $this->branches( self::MERGED );
// 		return array_search($branch, $branches) !== false;
// 	}
// 	public function end() {
// 		$res = chdir( $this->basePath );
// 	}
// }

// class StaticChecker {

// }

// class ExtensionMasterPolicy {

// 	private $masterExtensions = [];

// 	public function __construct() {
// 		$baseUrl = 'https://www.mediawiki.org/w/api.php?';
// 		$params = 'action=query&list=search&srsearch=' .
// 			urlencode( '"compatibility policy = master"' ) .
// 			'&srnamespace=102&srlimit=500&format=json';
// 		$response = file_get_contents( $baseUrl . $params );
// 		$json = json_decode( $response, true );
// 		$this->masterExtensions = array_map( function ( $val ) {
// 			return $val['title'];
// 		}, $json[ "query" ][ "search" ] );
// 	}

// 	public function isMasterPolicy( Extension $extension ): bool {
// 		return array_search( 'Extension:' . $extension->name(), $this->masterExtensions ) !== false;
// 	}
// }

// class ExtensionRequires {

// 	private function flatten( $dependencies ): array {
// 		$res = [];
// 		foreach ( $dependencies as $dependency => $version ) {
// 			if ( is_array( $version ) ) {
// 				$res = array_merge( $this->flatten( $version ) );
// 			} else {
// 				$res[ $dependency ] = $version;
// 			}
// 		}
// 		return $res;
// 	}

// 	private function formString( $dependencies ): string {
// 		$res = '';
// 		foreach ( $dependencies as $dependency => $version ) {
// 				$res .= $dependency . ' ' . $version . '; ';
// 		}
// 		return $res;
// 	}

// 	public function toString( Extension $extension ): string {
// 		return $this->formString( $this->toArray( $extension ) );
// 	}

// 	public function toArray( Extension $extension ): array {
// 		$extensionJson = $extension->extensionJson();
// 		$requires = $extensionJson["requires"] ?? [];
// 		return $this->flatten( $requires ) ;
// 	}
// }

// class ExtensionSymlink {
// 	private $basePath;

// 	public function __construct( string $basePath ) {
// 		$this->basePath = $basePath;
// 	}

// 	public function link( Extension $extension ): bool {
// 		$target = $this->basePath . '/extensions/' . $extension->name();
// 		if ( file_exists( $target ) ) {
// 			return symlink( $extension->path(), $target );
// 		}
// 		return true;
// 	}

// 	public function unlink( Extension $extension ): bool {
// 		$target = $this->basePath . '/extensions/' . $extension->name();
// 		if ( file_exists( $target ) ) {
// 			return unlink( $target );
// 		}
// 		return true;
// 	}
// }


function mergeInherited($processedClasses, $inherited, $classes): ?array {
	$res = [];
	foreach($inherited as $className) {
		if (!isset($processedClasses[$className])) {
			if ( isset($classes[$className]) ) {
				return null;
			}
			print("Absent class: " . $className . "\n");
			$res = array_merge($res, [$className]);
		}else {
			$res = array_merge($processedClasses[$className], $res, [$className]);
		}

	}
	return array_values(array_unique($res));
}


// interface ClassResgintry {
// 	function classes(string $byName);
// }


// class PhpClassDoc {
// 	/**
// 	 * @return PhpMethodDoc[]
// 	 */
// 	function methods(): array {
// 		return [];
// 	}
// }

// class PhpMethodDoc {


// }

// class PhpVarDoc {

// }

function execute() {
   	ExceptionHandler::installHandler();
	// CodeBase
	//    |---- Projects
	//       |---- Files
	//           |---- inf
	//           |---- php
	//           |---- composer

	// $extensions = [];
	// $masterExtensionsPolicy = new ExtensionMasterPolicy();




//	 $files = CodeBase::files( Projects::core( TestSettings::$coreDir ) )
//	 	->filter( new PhpFileFilter() );

//     $extensionsFiles = CodeBase::files( Projects::extension('/Users/ostapsmolar/projects/mediawiki/w/extensions' ) )
//     ->filter( New PhpFileFilter() );

	 $files = CodeBase::files( [
		 Projects::core( TestSettings::$coreDir ),
//		 Projects::extensions( TestSettings::$extensionsDir )
	 ] )->filter( New PhpFileFilter());

	 $classes = [];

	 foreach ($files as $file ) {
	 	print("Processing file: " . $file->path() . "\r\n" );
         /** @var PhpFile $file */
	 	$phpClasses = $file->classes();

	 	foreach ( $phpClasses as $class ) {
	 		try {
	 			$classes[$class->fullName()] = $class;
	 		} catch ( Exception $e ) {
	 			print( $e->getMessage() . "\r\n" );
	 		}
	 	}
	 }

	 $inheritsClasses = [ ];
	 $processedClasses = [];
	 while(count($classes) > 0) {
	 	foreach($classes as $className => $class) {
	 		$inh = array_merge($class->extends(), $class->implements());
	 		$sss = mergeInherited($inheritsClasses, $inh, $classes);
	 		if ($sss !== null) {
	 			$inheritsClasses[$className] = $sss;
	 			$processedClasses[$className] = $class;
	 			unset($classes[$className]);
	 		}
	 	}
	 }

	 foreach ($processedClasses as $className=>$_) {
	 	$derivedClasses[$className] = [];
	 }

	 foreach($inheritsClasses as $inheritsClass => $inherits) {
	 	foreach($inherits as $className) {
	 		$derivedClasses[$className] []= $inheritsClass;
	 	}
	 }

	 $processedMethods = [];
	 $overridesMethods = [];
	 $overriddenMethods = [];

	 foreach ($processedClasses as $className=>$class) {
	 	foreach($class->methods() as $methodName=>$method) {
	 		$processedMethods[$method->fullName()] = $method;
	 	}
	 }
	 foreach ($processedClasses as $className=>$class) {
	 	foreach($class->methods() as $methodName=>$method) {
	 		$overrides = [];

	 		foreach($inheritsClasses[$className] as $inherits) {
	 			if (isset($processedClasses[$inherits]) &&
	 				isset($processedClasses[$inherits]->methods()[$methodName])) {
	 				$overrides []= $processedClasses[$inherits]->methods()[$methodName]->fullName();
	 			}
	 		}

	 		$overridesMethods [$method->fullName()] = $overrides;
	 		$overridden = [];
	 		foreach($derivedClasses[$className] as $inherits) {
	 			if (isset($processedClasses[$inherits]) &&
	 				isset($processedClasses[$inherits]->methods()[$methodName])) {
	 				$overridden []= $processedClasses[$inherits]->methods()[$methodName]->fullName();
	 			}
	 		}
	 		$overriddenMethods [$method->fullName()] = $overridden;
	 	}
	 }

     foreach ($processedMethods as $method) {
         /** @var PhpMethod $method */
         $method->callees($processedClasses);
     }
     foreach ($processedMethods as $method) {
         foreach ($method->getCallees() as $calleePath => $returnValue) {
             /** @var PhpMethod[] $processedMethods  */
             if (array_key_exists($returnValue, $processedMethods)) {
                 $processedMethods[$returnValue]->addCaller($method->fullName());
             }
         }
     }

	 $connection = new MongoDB\Client (
	 	'mongodb+srv://admin:wmf-admin@wmf-cluster.tjsne.mongodb.net/classTreeDB?retryWrites=true&w=majority');
	 $classesCollection = $connection->selectCollection('classTreeDB', 'classes');
	 $classesCollection->drop();
    $propertiesCollection = $connection->selectCollection('classTreeDB', 'properties');
    $propertiesCollection->drop();
	 foreach($processedClasses as $className => $class) {
	 	try {
	 		print("Saving class: " . $className . "\r\n" );
	 		$arrayResult = Serializator::classToArray( $class, $inheritsClasses, $derivedClasses, $overridesMethods, $overriddenMethods );
	 		$classesCollection->insertOne($arrayResult);
            if (!empty($arrayResult['properties'])) {
                foreach ($arrayResult['properties'] as $property) {
                    $propertiesCollection->insertOne($property);
                }
            }
	 	} catch ( Exception $e ) {
	 		print( $e->getMessage() . "\r\n" );
	 	}
	 }

	 $methodsCollection = $connection->selectCollection('classTreeDB', 'methods');
	 $methodsCollection->drop();

	 foreach($processedMethods as $methodName => $method) {
	 	try {
	 		print("Saving method: " . $methodName . "\r\n" );
	 		$arrayResult = Serializator::methodToArray($method, $overridesMethods, $overriddenMethods);
	 		$methodsCollection->insertOne($arrayResult);
	 	} catch ( Exception $e ) {
	 		print( $e->getMessage() . "\r\n" );
	 	}
	 }


	print ("we're here");
//	$connection = new MongoDB\Client (
//		'mongodb+srv://admin:wmf-admin@wmf-cluster.tjsne.mongodb.net/classTreeDB?retryWrites=true&w=majority');
//	$classesCollection = $connection->selectCollection('classTreeDB', 'classes');
//
//	$res = $classesCollection->find([]);
//
//
//
//	foreach($res as $document) {
//		$phpClass = PhpClass::constructByArray($document);
//		print("Got class: " . $phpClass->fullName() . "\n");
//	}


	// $methods = CodeBase::methods( Projects::core("../../mediawiki") )->filter(
	// 	new HasClassInSignature( User::class )
	// );


	// foreach ($methods as $method) {
	// 	$users = $method->findIndetifiersInSignature( new OfType( User::class ) ); //['user']
	// 	foreach ($users as $user) {

	// 	}
	// 	$statements = $method->findStatements( new LineWith(user) );
	// 	$file->append($statements->toString([SETT::LineNumber,SETT::Text,  ]) )
	// }


	// $descedants = $files->filter( new IsDescedantOf( $files, ContentHandler::class ) );

	// $generator = new TestGenerator( new ContentHandlerAnalyzer() );

	// $files = new RecursiveIteratorIterator( $iterator );

}


execute();
